/*
Copyright (c) 2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _ADC_DRIVER_H
#define _ADC_DRIVER_H

#include "common.h"
#include "samc21.h"

typedef Adc ADC_PERIPHERAL;

#define IVREF_MODE_OFF     0
#define IVREF_MODE_1V024   SUPC_VREF_SEL_1V024
#define IVREF_MODE_2V048   SUPC_VREF_SEL_2V048
#define IVREF_MODE_4V096   SUPC_VREF_SEL_4V096

void IVREF_Setup(DWORD mode);

#define ADC_VREF_INTERNAL     ADC_REFCTRL_REFSEL_INTREF
#define ADC_VREF_VDD_DIV1_6   ADC_REFCTRL_REFSEL_INTVCC0
#define ADC_VREF_VDD_DIV2     ADC_REFCTRL_REFSEL_INTVCC1
#define ADC_VREF_EXTERNAL     ADC_REFCTRL_REFSEL_AREFA
#define ADC_VREF_DAC          ADC_REFCTRL_REFSEL_DAC
#define ADC_VREF_VDD          ADC_REFCTRL_REFSEL_INTVCC2

#define ADC_SPEED_MAX       0
#define ADC_SPEED_G0(x)    (x | 0x10)
#define ADC_SPEED_G2(x)    ((x) | 0x30)

#define ADC_CH_0          (0)
#define ADC_CH_1          (1)
#define ADC_CH_2          (2)
#define ADC_CH_3          (3)
#define ADC_CH_4          (4)
#define ADC_CH_5          (5)
#define ADC_CH_6          (6)
#define ADC_CH_7          (7)
#define ADC_CH_8          (8)
#define ADC_CH_9          (9)
#define ADC_CH_10         (10)
#define ADC_CH_11         (11)
#define ADC_CH_INTERF     (0x19)
#define ADC_CH_SVDDCORE   (0x1A)
#define ADC_CH_SVDDA      (0x1B)
#define ADC_CH_DAC        (0x1C)
#define ADC_CH_SVDD       (0x1D)

#define ADC_NEGCH_GND     (0x18)

#define ADC_SEQUENCE_DISABLE (0)
#define ADC_CH2SEQ(ch)       (1 << (ch))

#define ADC_INTERRUPT_RESULTREADY  ADC_INTENSET_RESRDY
#define ADC_INTERRUPT_OVERRUN      ADC_INTENSET_OVERRUN
#define ADC_INTERRUPT_WINMON       ADC_INTENSET_WINMON

#define ADC_AVERAGING_DISABLE        (0)
#define ADC_AVERAGING_AVERAGE        (1)
#define ADC_AVERAGING_ACCUMULATE     (2)
#define ADC_AVERAGING_OVERSAMPLE     (3)

#define ADC_AVERAGES_2     (0x01)
#define ADC_AVERAGES_4     (0x02)
#define ADC_AVERAGES_8     (0x03)
#define ADC_AVERAGES_16    (0x04)
#define ADC_AVERAGES_32    (0x05)
#define ADC_AVERAGES_64    (0x06)
#define ADC_AVERAGES_128   (0x07)
#define ADC_AVERAGES_256   (0x08)
#define ADC_AVERAGES_512   (0x09)
#define ADC_AVERAGES_1024  (0x0A)

#define ADC_OVERSAMPLE_13BITS (0x02)
#define ADC_OVERSAMPLE_14BITS (0x04)
#define ADC_OVERSAMPLE_15BITS (0x06)
#define ADC_OVERSAMPLE_16BITS (0x08)

#define ADC_WINMON_DISABLED                (0)
#define ADC_WINMON_GTLVALUE                (0x01)
#define ADC_WINMON_LTUVALUE                (0x02)
#define ADC_WINMON_GTLVALUE_AND_LTUVALUE   (0x03)
#define ADC_WINMON_LTLVALUE_AND_GTUVALUE   (0x04)

#define ADC_CONVERSION_SINGLE       (0)
#define ADC_CONVERSION_FREERUNNING  (1)

void ADC_Initialize(ADC_PERIPHERAL *instance, BYTE vref, BYTE speed);
void ADC_ChangeReference(ADC_PERIPHERAL *instance, BYTE vref);
void ADC_SetEvents(ADC_PERIPHERAL *instance, BYTE events); //TODO: This fuction needs #defines or needs to be reworked
void ADC_SetSingleEndedChannel(ADC_PERIPHERAL *instance, DWORD channel);
void ADC_SetDifferentialChannel(ADC_PERIPHERAL *instance, DWORD poschannel, DWORD negchannel);
void ADC_SetSequence(ADC_PERIPHERAL *instance, DWORD sequence);
void ADC_SetAveraging(ADC_PERIPHERAL *instance, DWORD mode, DWORD samples);
void ADC_SetWindowMonitor(ADC_PERIPHERAL *instance, DWORD mode, WORD lvalue, WORD uvalue);
void ADC_StartConversion(ADC_PERIPHERAL *instance, DWORD mode);
void ADC_StopConversion(ADC_PERIPHERAL *instance);

#define ADC_EnableInterrupt(instance, interrupt)          ((instance)->INTENSET.reg = (interrupt))
#define ADC_DisableInterrupt(instance, interrupt)         ((instance)->INTENCLR.reg = (interrupt))
#define ADC_FastSetSingleEndedChannel(instance, channel)  ((instance)->INPUTCTRL.reg = ADC_INPUTCTRL_MUXPOS((channel)))
#define ADC_FastStartConversion(instance)                 ((instance)->SWTRIG.bit.START = 1)
#define ADC_IsResultReady(instance)                       ((instance)->INTFLAG.bit.RESRDY == 1)  
#define ADC_GetSequenceState(instance)                    ((instance)->SEQSTATUS.bit.SEQSTATE)
#define ADC_IsSequenceBusy(instance)                      ((instance)->SEQSTATUS.bit.SEQBUSY == 1)
#define ADC_GetResult(instance)                           ((instance)->RESULT.reg)

#endif