/*
Copyright (c) 2017-2020 Boris Pertot. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef COMMON_FIFO_BUFFER_H
#define COMMON_FIFO_BUFFER_H

#include "basetypes.h"

typedef struct fifo_buffer{
	CHAR *Buffer;
	WORD MaxLenght;
	volatile WORD Lenght;
	volatile CHAR *Head, *Tail;
	DWORD (*LockFunction)(HANDLE);
	void (*UnlockFunction)(HANDLE, DWORD);
	HANDLE LockHandle;
	DWORD SyncOptions;
} FIFO_BUFFER;

#define LOCK_WRITESYNCHRONIZE 1
#define LOCK_WRITESTOP 0

#define FIFO_SYNCHRONIZE_NO 0
#define FIFO_SYNCHRONIZE_WRITES 1

void CreateStaticFifoBuffer(FIFO_BUFFER* handle, CHAR* memory, WORD size);
void FifoBuffer_SetLockFunc(FIFO_BUFFER* handle, DWORD (*lockfunc)(HANDLE), void (*unlockfunc)(HANDLE, DWORD), HANDLE lockhandle, DWORD syncoptions);
void FifoBuffer_Flush(FIFO_BUFFER* handle);
BOOLEAN FifoBuffer_FlushBytes(FIFO_BUFFER* handle, WORD len);
BOOLEAN FifoBuffer_WriteByte(FIFO_BUFFER* handle, CHAR data);
BOOLEAN FifoBuffer_WriteByteFromISR(FIFO_BUFFER* handle, CHAR data);
BOOLEAN FifoBuffer_WriteBytes(FIFO_BUFFER* handle, CHAR *data, WORD len);
BOOLEAN FifoBuffer_WriteString(FIFO_BUFFER* handle, CHAR *data);
BOOLEAN FifoBuffer_ReadByte(FIFO_BUFFER* handle, CHAR *data);
BOOLEAN FifoBuffer_ReadByteFromISR(FIFO_BUFFER* handle, CHAR *data);
BOOLEAN FifoBuffer_ReadBytes(FIFO_BUFFER* handle, CHAR *data, WORD len);
INT32 FifoBuffer_PeekBytesUntil(FIFO_BUFFER* handle, CHAR match, CHAR *data, WORD max_len);
WORD FifoBuffer_GetLenght(FIFO_BUFFER* handle);
WORD FifoBuffer_GetSize(FIFO_BUFFER* handle);

#endif /* COMMON_FIFO_BUFFER_H */
