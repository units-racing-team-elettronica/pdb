#include "SAMC21_BSP.h"
#include "PowerDist.h"


void PDB_Initialize(){
    //CAN Configuration
	GPIO_SetPinMode(GPIO_PORTB, 22, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinMux(GPIO_PORTB, 22, GPIO_PINMUX_CANSWD);
	GPIO_SetPinMode(GPIO_PORTB, 23, GPIO_PINMODE_INPUT);
	GPIO_SetPinMux(GPIO_PORTB, 23, GPIO_PINMUX_CANSWD);	
	MCAN_Initialize(&MCAN0);
	MCAN_ChangeMode(&MCAN0, MCAN_MODE_ENABLED);

	//ADC Configuration
	GPIO_SetPinMode(GPIO_PORTA, INV_F_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, INV_F_IMON, GPIO_PINMUX_ANALOG);
	GPIO_SetPinMode(GPIO_PORTA, INV_A_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, INV_A_IMON, GPIO_PINMUX_ANALOG);
	GPIO_SetPinMode(GPIO_PORTA, CLAX_IMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, CLAX_IMON, GPIO_PINMUX_ANALOG);
	GPIO_SetPinMode(GPIO_PORTA, LVS_VMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, LVS_VMON, GPIO_PINMUX_ANALOG);
	GPIO_SetPinMode(GPIO_PORTA, _12V_VMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, _12V_VMON, GPIO_PINMUX_ANALOG);
	GPIO_SetPinMode(GPIO_PORTA, SHDN_VMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, SHDN_VMON, GPIO_PINMUX_ANALOG);
	GPIO_SetPinMode(GPIO_PORTA, EXT_VMON, GPIO_PINMODE_ANALOG);
	GPIO_SetPinMux(GPIO_PORTA, EXT_VMON, GPIO_PINMUX_ANALOG);
	ADC_Initialize(ADC0, ADC_VREF_VDD, ADC_SPEED_MAX);
	ADC_Initialize(ADC1, ADC_VREF_VDD, ADC_SPEED_MAX);

	//GPIO Configuration
	GPIO_SetPinMode(GPIO_PORTA, INV_F_PEN, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinLow(GPIO_PORTA, INV_F_PEN); //INV_F not active
	GPIO_SetPinMode(GPIO_PORTA, INV_A_PEN, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinLow(GPIO_PORTA, INV_A_PEN); //INV_A not active
	GPIO_SetPinMode(GPIO_PORTA, CLAX_PEN, GPIO_PINMODE_OUTPUT);
	GPIO_SetPinLow(GPIO_PORTA, CLAX_PEN); //CLAX not active

	//Interrupt
	GPIO_SetPinMode(GPIO_PORTA, INV_F_nFAULT, GPIO_PINMODE_INPUT);
	GPIO_SetPinMux(GPIO_PORTA, INV_F_nFAULT, GPIO_PINMUX_EXTINT);
	GPIO_SetPinMode(GPIO_PORTA, INV_A_nFAULT, GPIO_PINMODE_INPUT);
	GPIO_SetPinMux(GPIO_PORTA, INV_A_nFAULT, GPIO_PINMUX_EXTINT);
	GPIO_SetPinMode(GPIO_PORTA, CLAX_nFAULT, GPIO_PINMODE_INPUT);
	GPIO_SetPinMux(GPIO_PORTA, CLAX_nFAULT, GPIO_PINMUX_EXTINT);

	EXTINT_Initialize(SYSTEM_CLOCK_SOURCE_1MHZ_GCLK1);
	EXTINT_ConfigureChannel(4, EXTINT_MODE_FALLING, TRUE, TRUE);
	EXTINT_ConfigureChannel(5, EXTINT_MODE_FALLING, TRUE, TRUE);
	EXTINT_ConfigureChannel(8, EXTINT_MODE_FALLING, TRUE, TRUE);
}



BOOLEAN MCAN_ComposeMessage(MCAN_MESSAGE *message, WORD Id, BYTE Length, WORD *value){
	if(Length > 8)
		return FALSE;
	message->Id = Id;
	message->Lenght = Length; //Lenght equal to the number of bytes
	for( BYTE i = 0; i < Length; i++){
		message->Data[i]= i%2 == 0 ?  (BYTE)(*(value+i/2) & 0xFF) : (BYTE)(*(value+i/2) >> 8);
	}
	return TRUE;
}
