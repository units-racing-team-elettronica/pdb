#ifndef _POWER_DISTRIBUTION_H
#define _POWER_DISTRIBUTION_H

#include "basetypes.h"


#define PDB_12V_VSCALE(x)	((x*11)/2)
#define PDB_LVS_VSCALE(x)		((x*23)/2)
#define PDB_INV_ISCALE(x)	((x*200)/279)

#define PDB_IMON_freq 50 //Current monitor sampling rate
#define PDB_VMON_freq 50 //Voltage monitor sampling rate
#define PDB_IMON_T (1000/PDB_IMON_freq) //Current monitor period in ms
#define PDB_VMON_T (1000/PDB_VMON_freq) //Voltage monitor period in ms

#define PDB_EXT_AVAILABLE_V 18000

#define PDB_INV_BACKOFF 5000 //Wait 5s before power cycling inverter 
#define PDB_MAX_INV_RTY 3 //Number of power cycles before disabling inverter

#define PDB_STATUS_MESSAGE_INTERVAL 100

#define PDB_ONLINE_CANID  0xC0//Board online
#define PDB_FAULT_CANID 0xC1//Fault
#define PDB_CURRENT0_CANID 0xCA//Currents value (Inverter_Front, Inverter_Aft, Clax)
#define PDB_CURRENT1_CANID 0XCB//Currents value (Safety/Ams, ECU, Dashboard/Tel/Comm) from I2C
#define PDB_VOLTAGE_CANID  0xCD//Voltage values (LVS, 12V, Ext, Shdn)
#define PDB_STATUS_CANID  0xCE//


#define ECU_STARTUP_CANID 0x50
#define ECU_KEEPALIVE_CANID 0x51


//PORT A pins
#define INV_F_IMON 4 
#define INV_A_IMON 5 
#define CLAX_IMON 6 
#define LVS_VMON 7
#define _12V_VMON 10
#define SHDN_VMON 11
#define INV_F_PEN 12
#define INV_A_PEN 13
#define SDA 16
#define SCL 17
#define CLAX_PEN 18
#define SHDNCVS_EN 19
#define INV_F_nFAULT 20
#define INV_A_nFAULT 21
#define SSB_SHDN_INHIB 25
#define CLAX_nFAULT 28

//PORT B pins
#define EXT_VMON 8
#define EXT_SEL 9
#define LED_1 10
#define LED_2 11


void PDB_Initialize();
BOOLEAN MCAN_ComposeMessage(MCAN_MESSAGE *message, WORD Id, BYTE Length, WORD *value);

#endif