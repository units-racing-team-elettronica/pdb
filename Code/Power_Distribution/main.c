#include "SAMC21_BSP.h"
#include "PowerDist.h"



//eFuse  variables
CHAR InvFFault = 0, InvAFault = 0, ClaxFault = 0;
BYTE InvFStatus = 0, InvAStatus = 0, ClaxStatus = 0;
volatile DWORD InvFFault_T = 0, InvAFault_T = 0, ClaxFault_T = 0;



int main(void)
{
	SYSTEM_InitializeClocks(SYSTEM_OSCILLATOR_EXTOSC);
	SYSTEM_InitalizeSysTick(SYSTEM_SYSTICK_FREQ);

	PDB_Initialize();

	// Temporization variables
	DWORD Actual, Last_Status, Last_Imon, Last_Vmon, LastLed1 = 0, LastLed2 = 0;
	Actual = Last_Status, Last_Imon = Last_Vmon = SYSTEM_GetSysTickCount();

	//Data variables
	WORD Imon[3] = {0,0,0}; //Fwd Inv current, Aft Inv current, CLAX current
	WORD Vmon[4] = {0,0,0,0}; //LVS, 12V, Ext, Shdn 
	BOOLEAN ExtAvailable = FALSE, Led1 = FALSE;

	//CAN variables
	MCAN_MESSAGE tx_message;
	MCAN_MESSAGE rx_message;
	BOOLEAN commState = 0; //send on can after ECU_STARTUP_CANID received
	
	MCAN_ComposeMessage(&tx_message, PDB_ONLINE_CANID, 0, NULL);
	MCAN_TransmitMessageFIFO(&MCAN0, &tx_message);

	BOOLEAN res;

	while(1) {
		Actual = SYSTEM_GetSysTickCount();

		//Can receive
		res = MCAN_ReadFIFOMessage(&MCAN0, 0, &rx_message);
		if(res){
			if(rx_message.Id == ECU_STARTUP_CANID){
				commState = TRUE;
				GPIO_SetPinHigh(GPIO_PORTA, INV_F_PEN);
				InvFStatus = 1;
				GPIO_SetPinHigh(GPIO_PORTA, INV_A_PEN);
				InvFStatus = 1;
				GPIO_SetPinHigh(GPIO_PORTA, CLAX_PEN);
				InvFStatus = 1;
			}
		}

		//send Status
		if(Actual - Last_Status >= PDB_STATUS_MESSAGE_INTERVAL){
			MCAN_MESSAGE msg = {0};
			msg.Id = PDB_STATUS_CANID;
			msg.Data[0] = InvFStatus;
			msg.Data[1] = InvAStatus;
			msg.Data[2] = ClaxStatus;
			msg.Data[3] = ExtAvailable;
			msg.Data[4] = GPIO_GetPinState(GPIO_PORTB, EXT_SEL);
			msg.Lenght = 5;
			MCAN_TransmitMessageFIFO(&MCAN0, &msg);
		}

		//eFuse Fault
		if(InvFFault_T > 0){
			if(commState){
					MCAN_MESSAGE msg = {0};
					msg.Id = PDB_FAULT_CANID;
					msg.Data[0] = 0;
					msg.Lenght = 1;
					MCAN_TransmitMessageFIFO(&MCAN0, &msg);
				}
			if(InvFFault == PDB_MAX_INV_RTY){
				GPIO_SetPinLow(GPIO_PORTA, INV_F_PEN);
				InvFStatus = 0;
				//InvFFault = 0;
				InvFFault_T = 0;
			}
			else{
				if(SYSTEM_GetSysTickCount() - InvFFault_T >= PDB_INV_BACKOFF){
					GPIO_SetPinLow(GPIO_PORTA, INV_F_PEN);
					GPIO_SetPinHigh(GPIO_PORTA, INV_F_PEN);
					InvFStatus = 1;
					InvFFault++;
					InvFFault_T = 0;
				}
			}
		}
		if(InvAFault_T > 0){
			if(commState){
					MCAN_MESSAGE msg = {0};
					msg.Id = PDB_FAULT_CANID;
					msg.Data[0] = 1;
					msg.Lenght = 1;
					MCAN_TransmitMessageFIFO(&MCAN0, &msg);
				}
			if(InvAFault == PDB_MAX_INV_RTY){
				GPIO_SetPinLow(GPIO_PORTA, INV_A_PEN);
				InvAStatus = 0;
				//InvAFault = 0;
				InvAFault_T = 0;
			}
			else{
				if(SYSTEM_GetSysTickCount() - InvAFault_T >= PDB_INV_BACKOFF){
					GPIO_SetPinLow(GPIO_PORTA, INV_A_PEN);
					GPIO_SetPinHigh(GPIO_PORTA, INV_A_PEN);
					InvAStatus = 1;
					InvAFault++;
					InvAFault_T = 0;
				}
			}
		}
		if(ClaxFault_T > 0){
			if(commState){
					MCAN_MESSAGE msg = {0};
					msg.Id = PDB_FAULT_CANID;
					msg.Data[0] = 2;
					msg.Lenght = 1;
					MCAN_TransmitMessageFIFO(&MCAN0, &msg);
				}
			if(ClaxFault == PDB_MAX_INV_RTY){
				GPIO_SetPinLow(GPIO_PORTA, CLAX_PEN);
				ClaxStatus = 0;
				//ClaxFault = 0;
				ClaxFault_T = 0;
			}
			else{
				if(SYSTEM_GetSysTickCount() - ClaxFault_T >= PDB_INV_BACKOFF){
					GPIO_SetPinLow(GPIO_PORTA, CLAX_PEN);
					GPIO_SetPinHigh(GPIO_PORTA, CLAX_PEN);
					ClaxStatus = 1;
					ClaxFault++;
					ClaxFault_T = 0;
				}
			}
		}

		//ADC readings		
		if(Actual - Last_Imon >= PDB_IMON_T){
			Last_Imon = Actual;

			ADC_SetSingleEndedChannel(ADC1,6); //PA4 for INV_F_IMON
			ADC_StartConversion(ADC1, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC1));
			Imon[0] = PDB_INV_ISCALE(ADC_GetResult(ADC1));

			ADC_SetSingleEndedChannel(ADC1,7); //PA5 for INV_A_IMON
			ADC_StartConversion(ADC1, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC1));
			Imon[1] = PDB_INV_ISCALE(ADC_GetResult(ADC1));

			ADC_SetSingleEndedChannel(ADC1,8); //PA6 for CLAX_IMON
			ADC_StartConversion(ADC1, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC1));
			Imon[2] = PDB_INV_ISCALE(ADC_GetResult(ADC1));

			if(commState){
				MCAN_ComposeMessage(&tx_message, PDB_CURRENT0_CANID, sizeof(Imon), Imon);
				MCAN_TransmitMessageFIFO(&MCAN0, &tx_message);
			}
		}
		if(Actual - Last_Vmon >= PDB_VMON_T){
			Last_Vmon = Actual;

			ADC_SetSingleEndedChannel(ADC0,7); //PA7 for LVS_VMON
			ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC0));
			Vmon[0] = PDB_LVS_VSCALE(ADC_GetResult(ADC0));

			ADC_SetSingleEndedChannel(ADC0,10); //PA10 for 12V_VMON
			ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC0));
			Vmon[1] = PDB_12V_VSCALE(ADC_GetResult(ADC0));

			ADC_SetSingleEndedChannel(ADC0,11); //PA11 for SHDN_VMON
			ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC0));
			Vmon[3] = PDB_LVS_VSCALE(ADC_GetResult(ADC0));

			ADC_SetSingleEndedChannel(ADC0,2); //PB08 for EXT_VMON
			ADC_StartConversion(ADC0, ADC_CONVERSION_SINGLE);
			while(!ADC_IsResultReady(ADC0));
			Vmon[2] = PDB_LVS_VSCALE(ADC_GetResult(ADC0));
			if(Vmon[2] >= PDB_EXT_AVAILABLE_V) 
				ExtAvailable = TRUE;
			else 
				ExtAvailable = TRUE;

			if(commState){
				MCAN_ComposeMessage(&tx_message, PDB_VOLTAGE_CANID, sizeof(Vmon), Vmon);
				MCAN_TransmitMessageFIFO(&MCAN0, &tx_message);
			}
		}
	
		//Leds
		if(ExtAvailable){ //Ext available/selected Led								
			if(!GPIO_GetPinState(GPIO_PORTB, EXT_SEL)){
				if(Actual - LastLed1 > 500){ //Ext available and selected -> blink 500ms
					LastLed1 = Actual;
					GPIO_TogglePin(GPIO_PORTB, LED_1);					
				}
			}else if(!Led1){
				GPIO_SetPinHigh(GPIO_PORTB, LED_1);
				Led1 = TRUE;
			}
		}else if(Led1){
			GPIO_SetPinLow(GPIO_PORTB, LED_1);				
			Led1 = FALSE;
		}
		//TODO: Led2

	}
}






void EIC_Handler(void){
	if (EIC->INTFLAG.reg == EIC_INTFLAG_EXTINT(4)){	//Interrupt on the INV_F_nFAULT
		EIC->INTFLAG.reg = EIC_INTFLAG_EXTINT(4);
		InvFFault_T = SYSTEM_GetSysTickCount();
	}
	if (EIC->INTFLAG.reg == EIC_INTFLAG_EXTINT(5)){	//Interrupt on the INV_A_nFAULT
		EIC->INTFLAG.reg = EIC_INTFLAG_EXTINT(5);
		InvAFault_T = SYSTEM_GetSysTickCount();
	}
	if (EIC->INTFLAG.reg == EIC_INTFLAG_EXTINT(8)){	//Interrupt on the CLAX_nFAULT
		EIC->INTFLAG.reg = EIC_INTFLAG_EXTINT(8);
		ClaxFault_T = SYSTEM_GetSysTickCount();
	}
}