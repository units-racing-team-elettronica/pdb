#ifndef _GLOBAL_CONFIG_H
#define _GLOBAL_CONFIG_H

//printf/sprintf options
//Notice: Enabling Floating point support will pull in software floating point libraries, increasing significantly code size
#define PRINTF_DISABLE_SUPPORT_FLOAT

//===== Peripheral Enable Section =====
#define MCAN0_ENABLED 1
#define MCAN1_ENABLED 0


//SYSTEM Module Configuration
#define SYSTEM_MAX_SYSTICK_CALLBACKS 4
#define SYSTEM_SYSTICK_FREQ 1000


#endif